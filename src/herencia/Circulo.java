
package herencia;

/**
 *
 * @author oskar
 */
public class Circulo extends Figura{
private double radio;
public Circulo(){
super();
radio=0;
}
public Circulo(String nombreFigura,double radio){
super(nombreFigura);
setRadio(radio);
}
public double getRadio(){
return radio;
}
public void setRadio(double radio){
this.radio=radio;
}
public void sacarperi(){
super.setPerimetro((2*getRadio())*Math.PI);
}
public void printCirculo(){
sacarperi();
System.out.println("la informacion del circulo es");
System.out.println(this);
}
@Override
public String toString(){
return "nombre de la figura es: "+super.getNombreFigura()+
"///// el radio es: "+getRadio()+
"//// el perimetro es: "+super.getPerimetro();


}
   
}

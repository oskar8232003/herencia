
package herencia;

/**
 *
 * @author oskar
 */
public class Triangulo extends Poligono{
String tipo;

public Triangulo(){
super();
tipo=new String(" ");
}

public String getTipo(){
return tipo;
}

public void setTipo(){
if((super.getTotal()/3)==super.geta(1)){
tipo="equilatero";
}else if( (( super.geta(0)==super.geta(1) )&&(super.geta(2)!=super.geta(0) )) ||
 (( super.geta(0)==super.geta(2) )&&(super.geta(1)!=super.geta(0) )) ||
  (( super.geta(1)==super.geta(2) )&&(super.geta(0)!=super.geta(1) )) ){
tipo="isosceles";
}else{
tipo="escaleno";
}
}

public void printTriangulo(){
setTipo();
System.out.println("informacion de triangulo");
System.out.println(toString());
}

@Override 
public String toString(){
return  "la figura es un: "+getTipo()
+"    su perimetro es: " + super.getTotal(); 
    }   

  
}
